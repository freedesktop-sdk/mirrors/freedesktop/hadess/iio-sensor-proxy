# Release instructions

1. Update [NEWS.md](./NEWS.md) with a list of changes.
2. Bump version in [meson.build](./meson.build).
3. Tag an annotated release: `git tag -s -m "X.Y" X.Y`.
